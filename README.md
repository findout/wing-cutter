# Wing cutter

A complete open source hot wire foam cutter for model airplane wings with all hardware and software.

## Background

A popular way of creating wings for model airplanes is to cut the wings out of blocks of polystyrene foam.
A common way is to use an electrically heated metal wire attached to a bow, pulled manually through the foam block.

To get the correct wing profile, a wing profile template is fastened on each side of the foam block. The hot wire is then pulled around the two profiles in a slow steady movement. It is not easy to get the wing surface smooth and correct this way. But it is simple cheap and it works.

Another way is a computer controlled machine that moves the wire throug the foam.

Google for "hot wire foam wing profile cutting" and you will see a lot of examples of both methods and machines.

Many of the machines however are either heavy, complicated, built on expensive components or just too weak and flimsy in my opinion.

# The Machine

[![the machine](wing-cutter-total.jpg)](https://youtu.be/1G55jCxNI1I)
(click the image to see a cutting demo video)

As you see, the cutter is built on two almost identical carriages with a wire between them. Each carriage position is ontrolled by two stepper motors. The four stepper motors are driven by a simple stepper driver board. The motion controller is a board with an ESP32 microcontroller.
The foam block to cut is placed on the two adjustable support bars.

The machine is built on cheap standard parts, that can be obtained from the nearest hardware store together with part for 3D printers. Many of the parts are 3D printed, so you should have access to a good 3D printer, capable of printing PLA.

## How to cut out a wing

I normally use Linux for everyhing, but as Fusion 360 doesn't run easily on Linux, I will use a Mac for this demo.

1. Put a foam block in the cutter and clamp it to the supports.
1. Select the wing profile to use. I will download a so called airfoil file from the [UIUC Airfoil Coordinates Database
](https://m-selig.ae.illinois.edu/ads/coord_database.html).
Click H and download [mh64.dat](https://m-selig.ae.illinois.edu/ads/coord/mh64.dat).
1. Start Fusion 360 with the Wingimport add-on installed. open a new empty design.
1. I click the Wingimporter icon to get the parameter dialog:
![Wingimporter dialog](dialog.png)
1. I enter all parameters:
    - path the mh64.dat file twice (root and tip profile)
    - root tip distance 700mm
    - root width 150mm, tip width 120mm
    - root margin 50mm - distance from wire start to where the wire goes into the foam block, tip margin 50mm - wire end foam distance
    - tip forward offset 0mm - wing front egde will be perpendicular to the flight direction
    - start dist above wing top 5mm - heigth of wire above the foam before cutting starts
    - Dist from foam top to wing top 5mm - how deep down in the foam the wing top is
    - dist from start to wing back 10mm - how log the wire will move horizontally before the actual wing tail edge end will start
    - hot wire cut diameter 5mm - diameter of the melted cavity around the wire as it cuts through the foam. This has to be determined experimentally with a test cut - see below.
    - heat power percent 50 - the percent of powering the wire with a constant current.50% means the power will be applied at 5KHz pulsing half time on and half time off.
1. Click OK. The wing shape will appear in the model area, for you to inspect.
![f360 wing](f360-wing.png)
1. You will find the file xxx.ngc in the xxx folder.
1. Turn on the power to the cutter and connect the ESP32 to the Mac USB connector.
1. Start cncjs on the Mac, select the ESP32 serial port and click Open.
1. Load the xxx.ngc so that you see the cutting path in the viewer. It will like this:
![cut path in cncjs](cncjs-cut-path.png)
1. Click run to start the cutting.
1. When ready, remove the foam block clamps, take off the foam block and push the new wing out of the block.

## Success factors

To get a wing with a smooth surface and the correct profile you have to have a good combination of the following parameters:

### wire tension along the entire cut

The wire must be kept straight along the whole cut, even when the carriages follow different paths - as they do if you cut a wing with different root and tip width.

### wire temperature

The wire should be heated enought to cut without really touching the foam. This way the wire will not bend due to forces on it digging through the foam.

### wire moving speed

When the wire aproaches the foam, it will start melting away before it touches and around the wire along the cutting path. The wire will cut away foam as if it were much thicker that the actual wire diameter. My wire is 0.5mm but the cutting diameter is 5mm. This cutting diameter depends on a combination of temperature and speed - hotter and slower means larger cutting diameter and vice versa.

### wire movement path

When the wire cuts through the foam, the foam above the cut will weight down alittle due to the elasticity of the foam. This means the you have to start cutting the upper wing surface and then go down and back along the bottom surface. You should also start from the back end of the profile in order to get the front right.

### software

To go from a wing profile file to the cutting path applied to your cutting machine, foam block size etc, requires a lot of parameters. To simplify this I created an add-on to the 3D cad program Fusion 360. It also adjusts the path to compensate for the effective cutting diameter explained above.

### Airfoil coordinate files

The airfoil mh64 mensioned earlier, look like this:

![mh64 air foil](wing-profile.png)
It can be downloaded as the file mh64.dat that contains 68 coordinates along the profile:

```
MH 64  8.59%
  1.00000000  0.00000000
  0.99677882 -0.00001741
  0.98710222  0.00007686
...
  0.99686985 -0.00020132
  1.00000000  0.00000000
```

The coordinates are scaled to make wing width 1. They are also more dense where the profile curves a lot - near the front, for example. So they can be used as they are for cuting, after some scaling and compensation for the cutting diameter:
![mh64 air foil expanded](wing-profile-expanded.png)

The Fusion 360 add-on handles all this. Here is an example of the cutting path for the mh64 profile:

## Fusion 360 Add-on

## Doing a cut

## Build one yourself
### Mechanics

### Motion controller

What we need is similar to whats inside a 3D printer.

#### A microcontroller

It interprets g-code and generates step pulses for four stepper motors and produces PWM pulses for the wire power switch. I use an ESP32 board loaded with the excellent [Grbl_Esp32](https://github.com/bdring/Grbl_Esp32) software.
The software should be configured for 5 axes, even if only 4 motors are controlled. I use axes X, Y, A and B.

This is the board I use:
![esp32-38pin](esp32-38pin.png)

#### Stepper motor drivers

For four NEMA 17 motors. I bougt a PCB - CNC Shield - made for an Arduino Uno that I connect to the ESP32 microcontroller PCB. I found a cheap shield with four A4988 drivers included.

The CNC shield I use:
![cnc shield](cnc-shield-drivers.jpg)

#### Wire power switch

MOS FET Switch capable of handling around 20V 15A, and that can be controlled by a PWM pulse output from the microcontroller.

This is the switch I use:
![power switch](mosfet-switch-15A.jpeg)


#### Power supply

A laptop charger suits well. The required voltage depends on the type and length of the heater wire. It will drive both the stepper motors and the wire. I use a 6.5A 19V charger, quite common with old heavy laptops.

#### Cutting wire

I currently use a Nicrothal 80 wire from Kanthal. You could use almost any wire normally used for heating elements, usually an alloy of iron, chrome and nickel, that has some resistance. Nicrothal 80 is around 5&ohm;/m, that makes 19V/5&ohm; = 3.8A if the wire is 1m.

### Software

I run an add-on on the Fusion360 CAD app, that generates a file containing g-code. G-code is a standard language for controlling CNC machines and 3D printers. We could call the wire cutter a CNC machine. CNC stands for Computerized Numeric Control.
I load the file into the CNCjs app on my Mac, and connects the mac to the ESP32 microcontroller with a USB-A to Micro USB cable. CNCjs let me send the g-code to the microcontroller that sends pulses to the stepper motors moving the wire through the foam block.

#### Generating g-code

I used Fusion 360 to generate g-code, but I could use a simle Pythonn script to generate it from wing profile files.
The advantage of using Fusion 360 is that you will see a model of the wing before you start the cutting. With the Python script you may not be able to see the wing before the cutting, so you may have to make some test cuts before you get the wing you want.

&lt;describe the F360 add-on and the Python script>

#### G-code sender

I use CNCjs to send the g-code to the microcontroller. But there are many other g-code senders that may work well for this. Google "g-code sender" and you will find a lot of different apps for this.
I found CNCjs easy to install and use. I used the desktop app downloaded from the page https://github.com/cncjs/cncjs/releases. There I found linux, mac and windows installation files.

#### ESP32 based motion controller

The requirement for the motion controller is to controll 4 stepper motors in coordinated motion, and to controll a PWM (Pulse Width Modulation) output for the power switch feeding the heating current to the heated wire.
ESP32 is a cheap microcontroller perfectly suited for this task. The brilliant g-code to stepper motor app [Grbl_Esp32](https://github.com/bdring/Grbl_Esp32) is a simple and powerful app. This app can be configured for different machine types with a configuration file. This file specifies which pins on the microcontroller should be used for which output signal. We need four pairs of signals to the stepper motor drivers and one PWM output signal.

Signals (only outputs):
* step X - pulse to step motor X one step
* direction X - step direction for motor X
* step Y
* direction Y
* step A
* direction A
* step B
* direction B
* Stepper enable - turns power on to all stepper motors
* PWM - turns wire power switch on/off at 5000Hz with varying on/off time to regulate power

&lt;esp32-drivers-motors schematic>

##### Preparing the ESP32 microcontroller with Grbl_Esp32

* Install VSCode
* Install PlatformIO
* Clone https://github.com/dagrende/Grbl_Esp32
* Open in VSCode
* Build
* Connect the ESP32 by USB cable
* Upload the resulting HEX file to the ESP32

It is possible to reassign the ESP32 module pins to get the signals to convenient pins.
This is done in the file Grbl_Esp32/src/Machines/hotcut.h.

### Electrical schematics

Using the building blocks described above.

